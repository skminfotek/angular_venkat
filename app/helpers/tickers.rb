require 'concurrent-edge'
Channel = Concurrent::Channel

## Go by Example: Tickers
# https://gobyexample.com/tickers

ticker = Channel.ticker(0.5)
Channel.go do
  ticker.each do |tick|
    print "Tick at #{tick}\n" if tick
  end
end

sleep(5.0)
ticker.stop
print "Ticker stopped\n"