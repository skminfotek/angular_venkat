// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
var MyController = function ($timeout) {
    var controller = this;


    var greet = function () {
        return "howdy";
    }

    var getFullName = function () {
        if (controller.firstName == '')
            controller.fullName = '';
        if (controller.lastName == '')
            controller.fullName = controller.firstName + '';
        else
            controller.fullName=controller.firstName+' '+ controller.lastName;
        if (controller.fullName == '')
            controller.fullName = "NA";
        return controller.fullName;
    }
    var action=function(msg){
        controller.count+=1;

        if(controller.firstName==='Sathish')
            alert('hi ' + msg);
        else
            alert("Not welcome");
    }

    controller.clickMe = function() {
        if(controller.actionPromise){
            console.log($timeout.cancel(controller.actionPromise));
        }
        controller.actionPromise=$timeout(action.bind(null,controller.firstName),3000);
    }
    controller.count=0;


    controller.firstName = '';
    controller.lastName = '';
    controller.fullName = '';
    controller.greet = greet;
    controller.getFullName = getFullName;


}
angular.module('myapp', []).controller('MyController', MyController);